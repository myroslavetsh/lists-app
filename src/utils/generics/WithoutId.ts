type WithoutId<T> = Omit<T, 'id'>

export default WithoutId
