import React, { useState } from 'react'

import { TaskAddingForm } from '@components/Form'
import { WithIconInteractive } from '@components/Interactive'
import { TaskBoxInput } from '@components/Input'
import { AddSVG } from '@components/SVGSprite'

import { ID, Task } from '@models/index'

import styles from './Styles.module.css'

type TaskPropTypes = {
  listId: ID
  list: Array<Task>
  setList: (item: Array<Task>) => void
}

const Tasks: React.FC<TaskPropTypes> = ({ listId, list, setList }) => {
  const [taskAddingVisible, setTaskAddingVisible] = useState<boolean>(false)

  const toggleTaskAddingVisibility = () => {
    setTaskAddingVisible(!taskAddingVisible)
  }

  const onTaskAdding = (newTask: Task) => {
    toggleTaskAddingVisibility()
    setList([...list, newTask])
  }

  const onTaskRemoving = (id: ID) => {
    setList(list.filter((task) => task.id !== id))
  }

  return (
    <>
      {list.length ? (
        <ul className={styles.list}>
          {list.map((task) => {
            return <TaskBoxInput task={task} key={task.id} onRemove={onTaskRemoving} />
          })}
        </ul>
      ) : (
        <div className={styles.emptyList}>Давайте добавим сюда первую таску 🐵</div>
      )}

      {taskAddingVisible ? (
        <TaskAddingForm
          listId={listId}
          onSuccess={onTaskAdding}
          onDeny={toggleTaskAddingVisibility}
        />
      ) : (
        <WithIconInteractive
          onClick={toggleTaskAddingVisibility}
          className={styles.add}
          iconStroked={true}
          icon={<AddSVG />}>
          Добавить задачу
        </WithIconInteractive>
      )}
    </>
  )
}

export default Tasks
