import React, { useEffect, useState } from 'react'
import ContentEditable, { ContentEditableEvent } from 'react-contenteditable'

import { CommonInteractivePropTypes } from '@components/Interactive'
import { CommonListItem, RemovableListItem } from '@components/ListItem'
import { CheckSVG } from '@components/SVGSprite'

import { ID, Task } from '@models/index'

import db from '@database/index'

import useDebounce from '@utils/hooks/useDebounce'

import styles from './Styles.module.css'

type TaskBoxPropTypes = {
  task: Task
  onRemove?: (id: ID) => void
}

// TODO: Consider empty field unavailability
const TaskBox: React.FC<TaskBoxPropTypes> = ({ task, onRemove }) => {
  const { id, text, completed } = task
  const [taskCompleted, setTaskCompleted] = useState<boolean>(completed)
  const [taskInputText, setTaskInputText] = useState<string>(text)
  const debouncedTaskInputText = useDebounce(taskInputText, 500)

  const toggleTackCompletedChecked = () => {
    db.update<Task>('tasks', id, { ...task, completed: !taskCompleted }).catch((error) =>
      console.error(error),
    )
    setTaskCompleted(!taskCompleted)
  }

  const handleTaskInputTextChange = (e: ContentEditableEvent) => {
    setTaskInputText(e.target.value)
  }

  useEffect(() => {
    db.update<Task>('tasks', id, { ...task, text: debouncedTaskInputText }).catch((error) =>
      console.error(error),
    )
  }, [debouncedTaskInputText, task, id])

  if (!onRemove) {
    return (
      <CommonListItem<CommonInteractivePropTypes>
        active={true}
        onClick={toggleTackCompletedChecked}
        className={styles.outer}>
        <label className={styles.taskLabel}>
          <input type='checkbox' checked={taskCompleted} readOnly />

          <div className={styles.divWithIcon}>
            <CheckSVG />

            <ContentEditable
              className={styles.textInput}
              html={taskInputText}
              disabled={false}
              onChange={handleTaskInputTextChange}
            />
          </div>
        </label>
      </CommonListItem>
    )
  }

  const removeTask = () => {
    db.delete<Task>('tasks', id)
      .then(() => {
        typeof onRemove === 'function' && onRemove(id)
      })
      .catch((error) => {
        console.error(error)
      })
  }

  return (
    <RemovableListItem<CommonInteractivePropTypes>
      active={true}
      onClick={toggleTackCompletedChecked}
      onRemove={removeTask}
      className={styles.outer}>
      <label className={styles.taskLabel}>
        <input type='checkbox' checked={taskCompleted} readOnly />

        <div className={styles.divWithIcon}>
          <CheckSVG />

          <ContentEditable
            className={styles.textInput}
            html={taskInputText}
            disabled={false}
            onChange={handleTaskInputTextChange}
          />
        </div>
      </label>
    </RemovableListItem>
  )
}

export default TaskBox
