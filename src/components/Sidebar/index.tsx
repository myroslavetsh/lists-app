import React, { useContext, useEffect, useState } from 'react'
import { Link, useNavigate } from 'react-router-dom'

import { WithIconInteractive } from '@components/Interactive'
import { RemovableListItem } from '@components/ListItem'
import { ListAddingForm } from '@components/Form'
import { CommonPopup } from '@components/Popup'
import { AddSVG, ListSVG } from '@components/SVGSprite'

import { ID, ToDoList } from '@models/index'

import db from '@database/index'

import ColorsContext from '@context/colors'

import compareStringLength from '@utils/compareStringLength'

import { DEFAULT_COLOR, MAXIMUM_SIDEBAR_ITEM_TEXT_LENGTH } from '@constants/'

import styles from './Styles.module.css'

export type SidebarPropTypes = {
  activeItemId: ID
  setActiveItemId: (index: ID) => void
  toDoLists: Array<ToDoList>
  setToDoLists: (item: Array<ToDoList>) => void
}

const Sidebar: React.FC<SidebarPropTypes> = ({
  activeItemId,
  setActiveItemId,
  toDoLists,
  setToDoLists,
}) => {
  const availableColors = useContext(ColorsContext)
  const [popupAddListVisible, setPopupAddListVisible] = useState<boolean>(false)
  const navigate = useNavigate()

  useEffect(() => {
    db.getWithSubscribe<Array<ToDoList>>('lists', setToDoLists)
  }, [setToDoLists])

  const showAddListPopup = () => {
    setPopupAddListVisible(true)
  }

  const hideAddListPopup = () => {
    setPopupAddListVisible(false)
  }

  const togglePopupVisibility = () => {
    if (popupAddListVisible) {
      hideAddListPopup()
    } else {
      showAddListPopup()
    }
  }

  const onListAdding = (id?: ID) => {
    hideAddListPopup()

    if (id) {
      navigate('/lists/' + id)
      setActiveItemId(id)
    } else {
      navigate('/')
      setActiveItemId('0')
    }
  }

  return (
    <div className={styles.sidebar}>
      <div className={styles.top}>
        <Link to='/'>
          <WithIconInteractive
            active={activeItemId === '0'}
            onClick={() => {
              setActiveItemId('0')
            }}
            icon={<ListSVG />}>
            Все списки
          </WithIconInteractive>
        </Link>
      </div>

      <ul className={styles.mid}>
        {toDoLists // Hottest lists get higher
          .sort((a, b) => Number(b.isHot) - Number(a.isHot))
          .map(({ id, name, isHot, colorId }) => {
            const comparedName = compareStringLength(name, MAXIMUM_SIDEBAR_ITEM_TEXT_LENGTH)
            const { hex } = availableColors.find(({ id }) => id === colorId) || DEFAULT_COLOR

            const onRemove = () => {
              db.delete<ToDoList>('lists', id)
                .then(() => {
                  setActiveItemId('0')
                  navigate('/')
                })
                .catch((error) => {
                  console.error(error)
                })
            }

            const ListItem = (
              <Link to={`/lists/${id}`}>
                <RemovableListItem
                  title={name}
                  color={hex}
                  isHot={isHot}
                  active={id === activeItemId}
                  onClick={() => setActiveItemId(id)}
                  onRemove={onRemove}
                  // TODO: Make fire as a variable
                  children={isHot ? comparedName + '🔥' : comparedName}
                />
              </Link>
            )

            return <React.Fragment key={id}>{ListItem}</React.Fragment>
          })}
      </ul>

      <div>
        <WithIconInteractive
          onClick={togglePopupVisibility}
          className={styles.add}
          iconStroked={true}
          icon={<AddSVG />}>
          Добавить список
        </WithIconInteractive>

        <CommonPopup visible={popupAddListVisible} onClose={hideAddListPopup} locked={false}>
          <ListAddingForm onAdd={onListAdding} />
        </CommonPopup>
      </div>
    </div>
  )
}

export default Sidebar
