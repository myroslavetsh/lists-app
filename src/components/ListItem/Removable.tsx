import React, { useState } from 'react'

import CommonItem, { CommonPropTypes } from './Common'
import { ConfirmationPopup } from '@components/Popup'
import {
  CommonInteractivePropTypes,
  WithColoredCircleInteractivePropTypes,
} from '@components/Interactive'
import { RemoveSVG } from '@components/SVGSprite'

import styles from './Styles.module.css'

type RemovablePropTypes<T extends CommonInteractivePropTypes> = CommonPropTypes<T> & {
  onRemove: () => void
  className?: string
}

function Removable<T extends CommonInteractivePropTypes>(props: RemovablePropTypes<T>) {
  const { onRemove, className, active, isHot, onClick, title, children } = props

  const [isConfirmationWindowVisible, setIsConfirmationWindowVisible] = useState<boolean>(false)

  const openConfirmationWindow = () => {
    setIsConfirmationWindowVisible(true)
  }

  const closeConfirmationWindow = () => {
    setIsConfirmationWindowVisible(false)
  }

  const classNames = [styles.outer]
  className && classNames.push(className)

  let Item = (
    <CommonItem<CommonInteractivePropTypes>
      active={active}
      isHot={isHot}
      onClick={onClick}
      title={title}
      children={children}
    />
  )

  if ('color' in props) {
    let color = props['color']

    Item = (
      <CommonItem<WithColoredCircleInteractivePropTypes>
        active={active}
        isHot={isHot}
        onClick={onClick}
        title={title}
        color={color}
        children={children}
      />
    )
  }

  return (
    <div className={classNames.join(' ')}>
      {Item}

      {active && (
        <>
          <button onClick={openConfirmationWindow}>
            <RemoveSVG />
          </button>

          <ConfirmationPopup
            visible={isConfirmationWindowVisible}
            onDeny={closeConfirmationWindow}
            onSuccess={() => {
              closeConfirmationWindow()
              typeof onRemove === 'function' && onRemove()
            }}
          />
        </>
      )}
    </div>
  )
}

Removable.defaultProps = {
  className: '',
}

export default Removable
