import React, { useEffect, useState } from 'react'

import { TaskBoxInput } from '@components/Input'

import { ExpandedList } from '@models/index'

import db from '@database/index'

import styles from './Styles.module.css'

const Main: React.FC = () => {
  const [expandedLists, setExpandedLists] = useState<Array<ExpandedList>>([])

  useEffect(() => {
    db.getExpandedLists(setExpandedLists)
  }, [])

  return (
    <main className={styles.flex}>
      {expandedLists.map((item) => (
        <React.Fragment key={item.id}>
          {
            // TODO: Move styles to css module
            <div style={{ display: 'flex', flexDirection: 'column' }}>
              <h2
                style={{
                  backgroundColor: item.color.hex,
                }}
                className={styles.heading}>
                {item.name}
                {item.isHot && '🔥'}
              </h2>

              {item.tasks.length ? (
                <ul className={styles.list}>
                  {item.tasks.map((task) => {
                    return <TaskBoxInput task={task} key={task.id} />
                  })}
                </ul>
              ) : (
                <div> Нет тасок :&#40; </div>
              )}
            </div>
          }
        </React.Fragment>
      ))}
    </main>
  )
}

export default Main
