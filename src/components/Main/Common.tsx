import React, { useEffect, useReducer, useState } from 'react'
import { useParams } from 'react-router-dom'
import ContentEditable, { ContentEditableEvent } from 'react-contenteditable'

import { TaskList } from '@components/List'
import { SidebarPropTypes } from '@components/Sidebar'
import { CheckSVG, PencilSVG } from '@components/SVGSprite'
import { ListSettingsForm } from '@components/Form'

import { ColorDTO, ToDoList, Task, ExpandedList } from '@models/index'

import db from '@database/index'

import styles from './Styles.module.css'

type MainPropTypes = Pick<SidebarPropTypes, 'activeItemId' | 'toDoLists' | 'setToDoLists'>

enum ReducerActionType {
  CHANGE_IS_HOT,
  CHANGE_TITLE_COLOR,
  CHANGE_LIST_NAME,
  CHANGE_LIST_ID,
  CHANGE_HEADING_EDITABLE,
  CHANGE_WHOLE_LIST,
}

type State = {
  titleColorId: number
  titleColor: string
  listName: string
  listId: number
  isHeadingEditable: boolean
}

type ReducerAction = {
  type: ReducerActionType
  payload?: any
}

//TODO: Refactor with simple state
const reducer = (state: State, action: ReducerAction) => {
  switch (action.type) {
    case ReducerActionType.CHANGE_IS_HOT:
      return { ...state, isHot: action.payload }
    case ReducerActionType.CHANGE_TITLE_COLOR:
      return { ...state, ...action.payload }
    case ReducerActionType.CHANGE_LIST_NAME:
      return { ...state, listName: action.payload }
    case ReducerActionType.CHANGE_LIST_ID:
      return { ...state, listId: action.payload }
    case ReducerActionType.CHANGE_HEADING_EDITABLE:
      return { ...state, isHeadingEditable: action.payload }
    case ReducerActionType.CHANGE_WHOLE_LIST:
      return { ...state, ...action.payload }
    default:
      return state
  }
}

const Main: React.FC<MainPropTypes> = ({
  activeItemId: activeSidebarItemId,
  toDoLists,
  setToDoLists,
}) => {
  const { id: currentListId } = useParams()
  const [currentList, setCurrentList] = useState<ToDoList>({} as ToDoList)
  const [expandedLists, setExpandedLists] = useState<Array<ExpandedList>>([])
  const [currentTasks, setCurrentTasks] = useState<Array<Task>>([])
  const [{ titleColorId, titleColor, listName, listId, isHeadingEditable, isHot }, dispatch] =
    useReducer(reducer, {
      titleColorId: 1,
      titleColor: '#000',
      listName: 'Загружаемса...',
      listId: 0,
      isHeadingEditable: false,
      isHot: false,
    })

  useEffect(() => {
    db.getExpandedLists(setExpandedLists)
    db.read<ToDoList>('lists/' + currentListId)
      .then((list) => {
        setCurrentList(list)
      })
      .catch((error) => {
        console.error(error)
      })
  }, [activeSidebarItemId, currentListId])

  useEffect(() => {
    const compare: (list: ExpandedList) => boolean = ({ id }) => id === activeSidebarItemId

    if (expandedLists.filter(compare)[0]) {
      const { tasks, color, name, id, isHot } = expandedLists.filter(compare)[0]

      setCurrentTasks(tasks)

      dispatch({
        type: ReducerActionType.CHANGE_WHOLE_LIST,
        payload: {
          titleColorId: color.id,
          titleColor: color.hex,
          listName: name,
          listId: id,
          isHot,
        },
      })
    }
  }, [activeSidebarItemId, expandedLists])

  const handleCurrentListNameChange = (e: ContentEditableEvent) => {
    dispatch({ type: ReducerActionType.CHANGE_LIST_NAME, payload: e.target.value })
  }

  const handleChangeButtonClick = () => {
    if (isHeadingEditable) {
      setToDoLists([
        ...toDoLists.map((item) => {
          if (item.id === listId) {
            item.name = listName
          }

          return item
        }),
      ])

      if (currentListId) {
        db.update('lists', currentListId, { ...currentList, name: listName }).catch((error) =>
          console.error(error),
        )
      }
    }

    dispatch({ type: ReducerActionType.CHANGE_HEADING_EDITABLE, payload: !isHeadingEditable })
  }

  const onCloseOptions = () => {
    // TODO: Compare with settings and if no changes - do not send the request
    if (currentListId) {
      db.update('lists', currentListId, {
        ...currentList,
        colorId: parseInt(titleColorId),
        isHot,
      }).catch((error) => console.error(error))
    }
  }

  const headingClassNames = [styles.heading]
  isHeadingEditable && headingClassNames.push(styles.editable)

  return (
    <main className={styles.main}>
      {
        <>
          <h2
            style={{
              backgroundColor: titleColor,
            }}
            className={headingClassNames.join(' ')}>
            <ContentEditable
              className={styles.textInput}
              html={listName}
              disabled={!isHeadingEditable}
              onChange={handleCurrentListNameChange}
            />

            {/* TODO: лоадер на кнопку и типа крестика если не удалось запостить или невалидное значение */}
            <button type='button' className={styles.edit} onClick={handleChangeButtonClick}>
              {isHeadingEditable ? <CheckSVG /> : <PencilSVG />}
            </button>
          </h2>

          {expandedLists.length && (
            <TaskList
              listId={listId}
              list={Array.isArray(currentTasks) ? currentTasks : []}
              setList={setCurrentTasks}
            />
          )}
        </>
      }

      <ListSettingsForm
        isHot={isHot}
        onColorChoose={async ({ id, hex }: ColorDTO) => {
          dispatch({
            type: ReducerActionType.CHANGE_TITLE_COLOR,
            payload: {
              titleColorId: id,
              titleColor: hex,
            },
          })
        }}
        chosenColor={{ id: titleColorId, hex: titleColor }}
        onHotChange={() => {
          dispatch({ type: ReducerActionType.CHANGE_IS_HOT, payload: !isHot })
        }}
        onCloseOptions={onCloseOptions}
      />
    </main>
  )
}

export default Main
