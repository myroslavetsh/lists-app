import { useContext } from 'react'

import { ColorDTO } from '@models/Color'

import ColorContext from '@context/colors'

import styles from './Styles.module.css'

type PropTypes = {
  active: ColorDTO
  setActive: (color: ColorDTO) => void
}

const ColorPicker: React.FC<PropTypes> = ({ active, setActive }) => {
  const availableColors = useContext(ColorContext)

  return (
    <ul className={styles.ul}>
      {availableColors.map((color) => {
        const classNames = [styles.circle]
        active.id === color.id && classNames.push(styles.active)

        return (
          <li
            key={color.id}
            className={classNames.join(' ')}
            title={color.name}
            style={{ backgroundColor: color.hex }}
            onClick={() => setActive(color)}
          />
        )
      })}
    </ul>
  )
}

export default ColorPicker
