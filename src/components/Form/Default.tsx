import React from 'react'

import styles from './Styles.module.css'

const Default: React.FC = ({ children }) => {
  return <form className={styles.form}>{children}</form>
}

export default Default
