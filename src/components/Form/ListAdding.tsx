import React, { ChangeEvent, useState } from 'react'
import { uid } from 'uid'

import { CommonInput, CheckBoxInput } from '@components/Input'
import Button from '@components/Button'
import { ColorPickerForm, DefaultForm } from '.'

import { ID, ToDoListWithoutId, ColorDTO, ToDoList } from '@models/index'

import db from '@database/index'

import { DEFAULT_COLOR } from '@utils/constants'

type ListAddingPropTypes = {
  onAdd: (id?: ID) => void
}

const ListAdding: React.FC<ListAddingPropTypes> = ({ onAdd }) => {
  // TODO: Rewrite on useReducer Input fields
  const [nameValue, setNameValue] = useState<string>('')
  const [nameValueValid, setNameValueValid] = useState<boolean>(true)
  const [activeColor, setActiveColor] = useState<ColorDTO>(DEFAULT_COLOR)
  const [isHotChecked, setIsHotChecked] = useState<boolean>(false)

  const [isAddingInProcess, setIsAddingInProcess] = useState<boolean>(false)

  const handleNameInputChange = (event: ChangeEvent<HTMLInputElement>) => {
    if (event.target.value.trim() === '') {
      setNameValueValid(false)
      setNameValue('')
    } else {
      setNameValueValid(true)
      setNameValue(event.target.value)
    }
  }

  const handleIsHotInputChange = () => {
    setIsHotChecked(!isHotChecked)
  }

  //FIXME: Make a name checking to avoid repetitions
  const addList = ({ name, colorId, isHot }: ToDoListWithoutId) => {
    if (!(name.trim() === '')) {
      const id = uid()
      const toDoList = { id, name, colorId, isHot }
      setIsAddingInProcess(true)

      setNameValue('')
      setActiveColor(DEFAULT_COLOR)
      setIsHotChecked(false)
      setIsAddingInProcess(false)

      db.create<ToDoList>('lists', id, toDoList)
      onAdd(id)
    } else {
      setNameValueValid(false)
    }
  }

  const nameInputPlaceholder = nameValueValid ? 'Название списка' : 'Введите корректое название'
  const checkBoxInputPlaceholder = 'Пометить как важный' + (isHotChecked ? ' 🔥' : '')

  return (
    <DefaultForm>
      <CommonInput
        placeholder={nameInputPlaceholder}
        value={nameValue}
        valid={nameValueValid}
        onChange={handleNameInputChange}
      />

      <CheckBoxInput
        placeholder={checkBoxInputPlaceholder}
        checked={isHotChecked}
        onChange={handleIsHotInputChange}
      />

      <ColorPickerForm active={activeColor} setActive={setActiveColor} />

      <Button
        type='button'
        color='#08D11C'
        disabled={isAddingInProcess}
        fullWidth={true}
        textCenter={true}
        onClick={() => {
          addList({
            name: nameValue,
            colorId: activeColor.id,
            isHot: isHotChecked,
          })
        }}>
        {!isAddingInProcess ? 'Добавить' : 'Добавление...'}
      </Button>
    </DefaultForm>
  )
}

export default ListAdding
