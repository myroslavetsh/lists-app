import React, { useState } from 'react'
import { uid } from 'uid'

import { CommonInput } from '@components/Input'
import Button from '@components/Button'
import DefaultForm from './Default'

import { Task, ID } from '@models/index'

import db from '@database/index'

import styles from './Styles.module.css'

type TaskAddingPropTypes = {
  listId: ID
  onSuccess: (task: Task) => void
  onDeny: () => void
}

const TaskAdding: React.FC<TaskAddingPropTypes> = ({ listId, onSuccess, onDeny }) => {
  const [text, setText] = useState<string>('')
  const [textValid, setTextValid] = useState<boolean>(true)

  const addTask = () => {
    if (text.trim() !== '') {
      const id = uid()
      const task = { id, listId, text, completed: false }

      db.create<Task>('tasks', id, task)
        .then(() => {
          onSuccess(task)
        })
        .catch((error) => {
          console.error(error)
        })
    } else {
      setTextValid(false)
    }
  }

  return (
    <DefaultForm>
      <CommonInput
        placeholder={textValid ? 'Текст задачи' : 'Даёшь корректные названия!!!'}
        value={text}
        onChange={(e) => {
          setText(e.target.value)
        }}
        valid={textValid}
      />

      <div className={styles.buttons}>
        <Button onClick={addTask} color='#08D11C' type='button' children='Добавить задачу' />
        <Button onClick={onDeny} color='#D10808' type='button' children='Отмена' />
      </div>
    </DefaultForm>
  )
}

export default TaskAdding
