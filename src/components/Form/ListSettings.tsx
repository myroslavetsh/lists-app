import { useState } from 'react'

import { ColorPickerForm } from '.'
import { FireSVG, SettingsSVG } from '@components/SVGSprite'

import { ColorDTO } from '@models/Color'

import styles from './Styles.module.css'

type PropTypes = {
  isHot: boolean
  chosenColor: ColorDTO
  onHotChange: () => void
  onCloseOptions: () => void
  onColorChoose: (color: ColorDTO) => void
}

const ListSettings: React.FC<PropTypes> = ({
  chosenColor,
  onColorChoose,
  isHot,
  onHotChange,
  onCloseOptions,
}) => {
  const [optionsVisible, setOptionsVisible] = useState<boolean>(false)

  const toggleOptionsVisible = () => {
    optionsVisible && onCloseOptions()

    setOptionsVisible(!optionsVisible)
  }

  const hotClassNames = [styles.hot]
  !isHot && hotClassNames.push(styles.passiveHot)

  return (
    <div className={styles.settings}>
      <button onClick={toggleOptionsVisible}>
        <SettingsSVG />
      </button>
      {optionsVisible && (
        <div className={styles.options}>
          <button onClick={onHotChange} className={hotClassNames.join(' ')}>
            <FireSVG />
          </button>

          <ColorPickerForm active={chosenColor} setActive={onColorChoose} />
        </div>
      )}
    </div>
  )
}

export default ListSettings
