import { child, get, onValue, ref, remove, set, update } from 'firebase/database'

import { Color, ExpandedList, ID, Task, ToDoList } from '@models/index'

// TODO: Make a path in tsconfig for constants
import { DEFAULT_COLOR } from '@utils/constants'

import { initializeApp } from 'firebase/app'
import { getDatabase } from 'firebase/database'

class DataBase {
  private db

  constructor(config: any) {
    this.db = getDatabase(initializeApp(config))
  }

  async create<T extends object>(path: string, id: ID, entity: T): Promise<void> {
    await set(ref(this.db, `${path}/${id}`), entity)
  }

  async read<T extends object>(path: string): Promise<T> {
    return (await get(child(ref(this.db), path))).val()
  }

  async update<T extends object>(path: string, id: ID, entity: T): Promise<void> {
    await update(ref(this.db, `${path}/${id}`), entity)
  }

  async delete<T extends { id: ID }>(path: string, id: ID | Pick<T, 'id'>): Promise<void> {
    remove(ref(this.db, `${path}/${id}`))
  }

  async getWithSubscribe<T extends object>(
    path: string,
    setState: (state: T) => void,
  ): Promise<void> {
    onValue(ref(this.db, path), (snapshot) => {
      //@ts-ignore
      setState(Object.values(snapshot.val()))
    })
  }

  async getExpandedLists(setExpandedLists: (lists: Array<ExpandedList>) => void): Promise<void> {
    const lists: Array<ToDoList> = Object.values(
      await get(child(ref(this.db), 'lists')).then((snapshot) => snapshot.val()),
    )
    const tasks: Array<Task> = Object.values(
      await get(child(ref(this.db), 'tasks')).then((snapshot) => snapshot.val()),
    )
    const colors: Array<Color> = Object.values(
      await get(child(ref(this.db), 'colors')).then((snapshot) => snapshot.val()),
    )

    setExpandedLists(
      lists.map((list) => {
        return {
          ...list,
          color: colors.find(({ id }) => list.colorId === id) || DEFAULT_COLOR,
          tasks: tasks.filter(({ listId }) => listId === list.id),
        }
      }),
    )
  }
}

export default new DataBase({
  apiKey: process.env.REACT_APP_API_KEY,
  authDomain: 'to-do-simplified.firebaseapp.com',
  databaseURL: 'https://to-do-simplified-default-rtdb.firebaseio.com',
  projectId: 'to-do-simplified',
  storageBucket: 'to-do-simplified.appspot.com',
  messagingSenderId: '138592522962',
  appId: '1:138592522962:web:014dd6a44313a9a95a530b',
})
