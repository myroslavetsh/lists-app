import { ID } from '.'

import { WithoutId } from '@utils/generics'

export default interface Task {
  id: ID
  listId: ID
  text: string
  completed: boolean
}

export type TaskWithoutId = WithoutId<Task>
