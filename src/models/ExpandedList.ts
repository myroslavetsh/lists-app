import { ToDoList, Color, Task } from '.'

type ExpandedList = ToDoList & { color: Color; tasks: Array<Task> }

export default ExpandedList
