import { ID } from '.'

export default interface Color {
  id: ID
  hex: string
  name: string
}

export type ColorDTO = Pick<Color, 'hex' | 'id'>
