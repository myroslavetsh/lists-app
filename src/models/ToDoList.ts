import { ID } from '.'

import { WithoutId } from '@utils/generics'

export default interface ToDoList {
  id: ID
  colorId: ID
  name: string
  isHot: boolean
}

export type ToDoListWithoutId = WithoutId<ToDoList>
