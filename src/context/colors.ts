import React from 'react'

import Color from '@models/Color'

export default React.createContext<Array<Color>>([])
