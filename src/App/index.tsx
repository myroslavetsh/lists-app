import React, { useEffect, useState } from 'react'
import { Routes, Route, useParams } from 'react-router-dom'

import { AllMain, CommonMain } from '@components/Main'
import Sidebar from '@components/Sidebar'

import { ID, ToDoList, Color } from '@models/index'

import db from '@database/index'

import ColorsContext from '@context/colors'

type Props = {
  todoLists: Array<ToDoList>
  setTodoLists: (items: Array<ToDoList>) => void
  activeSidebarItemId: ID
  setActiveSidebarItemId: (id: ID) => void
}

const ListWithId: React.FC<Props> = ({
  todoLists,
  setTodoLists,
  activeSidebarItemId,
  setActiveSidebarItemId,
}) => {
  const { id } = useParams()

  useEffect(() => {
    //@ts-ignore
    setActiveSidebarItemId(id)
  }, [setActiveSidebarItemId, id])

  return (
    <div className='page'>
      <Sidebar
        toDoLists={todoLists}
        setToDoLists={setTodoLists}
        activeItemId={activeSidebarItemId}
        setActiveItemId={setActiveSidebarItemId}
      />
      <CommonMain
        activeItemId={activeSidebarItemId}
        toDoLists={todoLists}
        setToDoLists={setTodoLists}
      />
    </div>
  )
}

const App: React.FC = () => {
  const [activeSidebarItemId, setActiveSidebarItemId] = useState<ID>('0') //TODO: Replace with ALL_ITEMS_ID const It's showing all lists
  const [toDoLists, setToDoLists] = useState<Array<ToDoList>>([])
  const [colors, setColors] = useState<Array<Color>>([])

  useEffect(() => {
    db.read<Array<Color>>('colors')
      .then((colors) => {
        setColors(colors)
      })
      .catch((error) => {
        console.error(error)
      })
  }, [])

  return (
    <ColorsContext.Provider value={colors}>
      <Routes>
        <Route
          path='/'
          element={
            <div className='page'>
              <Sidebar
                toDoLists={toDoLists}
                setToDoLists={setToDoLists}
                activeItemId={activeSidebarItemId}
                setActiveItemId={setActiveSidebarItemId}
              />
              <AllMain />
            </div>
          }
        />
        <Route
          path='/lists/:id'
          element={
            <ListWithId
              todoLists={toDoLists}
              setTodoLists={setToDoLists}
              activeSidebarItemId={activeSidebarItemId}
              setActiveSidebarItemId={setActiveSidebarItemId}
            />
          }
        />
      </Routes>
    </ColorsContext.Provider>
  )
}

export default App
